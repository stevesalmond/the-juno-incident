Shader "Custom/Halo"
{
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Color ("Color", Color) = (1, 1, 1, 1)
		_Brightness ("Brightness", Float) = 1
		_FadeStart ("Fade Start", Float) = 100.0
		_InverseFadeDistance ("Inverse Fade Distance", Float) = 0.025
	}
	
	SubShader
	{
		LOD 200

		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Blend One One
		Cull Off Lighting Off ZWrite Off
		ZTest Always
		  
		Fog { Mode Off }
		
		CGPROGRAM
		#pragma surface surf Unlit vertex:vert
		
		sampler2D _MainTex;
		float4 _Color;
		float _Brightness;
		float _FadeStart;
		float _InverseFadeDistance;
				
		struct Input
		{
			float2 uv_MainTex;
			half alpha;
		};
		
		// Unlit.
		fixed4 LightingUnlit(SurfaceOutput s, fixed3 lightDir, fixed atten)
	    {
	        fixed4 c;
	        c.rgb = s.Albedo; 
	        c.a = s.Alpha;
	        return c;
	    }
	    		
		void vert (inout appdata_full v, out Input o)
		{
			float3 d = mul(UNITY_MATRIX_MVP, v.vertex).xyz;
			o.alpha = lerp(1, 0, (d.z - _FadeStart) * _InverseFadeDistance);
			o.uv_MainTex = v.texcoord.xy;
		}
		
		void surf (Input IN, inout SurfaceOutput o)
		{
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) ;
			o.Albedo = c.rgb * _Color.rgb * _Brightness * IN.alpha;
		}
		
		ENDCG
	}
	
	Fallback "VertexLit"
}