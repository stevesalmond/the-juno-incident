using UnityEngine;
using UnityEditor;
using System.Collections;

/** A custom Unity editor for planet snapping. */
[CustomEditor(typeof(SphereSnap))]
[CanEditMultipleObjects]
public class SphereSnapEditor : Editor
{
	
	// Members
	// ----------------------------------------------------------------------------
	
	/** Azimuth property. */
	private SerializedProperty azimuthProp;

	/** Altitude property. */
	private SerializedProperty altitudeProp;
	
	/** Whether to snap altitude. */
	private SerializedProperty altitudeSnapProp;

	/** Whether to randomize this object. */
	private SerializedProperty randomizedProp;
	
	/** Random azimuth range property. */
	private SerializedProperty randomAzimuthMinProp;

	/** Random azimuth range property. */
	private SerializedProperty randomAzimuthMaxProp;

	/** Random scale range property. */
	private SerializedProperty randomScaleMinProp;

	/** Random scale range property. */
	private SerializedProperty randomScaleMaxProp;
	
	/** Whether to show randomization options. */
	private bool showRandomizationOptions = false;

	
	// Unity Implementation
	// ----------------------------------------------------------------------------
	
	/** Handles the inspector being enabled. */
	public void OnEnable()
	{
		azimuthProp = serializedObject.FindProperty("Azimuth");	
		altitudeProp = serializedObject.FindProperty("Altitude");
		altitudeSnapProp = serializedObject.FindProperty("AltitudeSnap");
		randomizedProp = serializedObject.FindProperty("Randomized");
		randomAzimuthMinProp = serializedObject.FindProperty("RandomAzimuthMin");
		randomAzimuthMaxProp = serializedObject.FindProperty("RandomAzimuthMax");
		randomScaleMinProp = serializedObject.FindProperty("RandomScaleMin");
		randomScaleMaxProp = serializedObject.FindProperty("RandomScaleMax");
	}

	/** Updates the Unity inspector GUI. */
	public override void OnInspectorGUI()
	{
		// Update the serialized property.
		serializedObject.Update();
		
		// Display custom GUI for editing the snapping properties.
		EditorGUILayout.Slider(azimuthProp, 0, 360, new GUIContent("Azimuth"));
		GUILayout.Space(5);
		altitudeSnapProp.boolValue = EditorGUILayout.Toggle("Altitude Snap", altitudeSnapProp.boolValue);
		EditorGUILayout.Slider(altitudeProp, -2, 2, new GUIContent("Altitude"));
		GUILayout.Space(5);
		
		showRandomizationOptions = EditorGUILayout.Foldout(showRandomizationOptions, "Randomization Options");
		if (showRandomizationOptions)
		{
			randomizedProp.boolValue = EditorGUILayout.Toggle("Randomized", randomizedProp.boolValue);
			EditorGUILayout.Slider(randomAzimuthMinProp, 0, 360, new GUIContent("Min Azimuth"));
			EditorGUILayout.Slider(randomAzimuthMaxProp, 0, 360, new GUIContent("Max Azimuth"));
			EditorGUILayout.Slider(randomScaleMinProp, 0.1f, 5.0f, new GUIContent("Min Scale"));
			EditorGUILayout.Slider(randomScaleMaxProp, 0.1f, 5.0f, new GUIContent("Max Scale"));
		}
		
		if (GUILayout.Button("Randomize", GUILayout.Height(24)))
		{
			// Randomly alter each selected snap object.
			foreach (SphereSnap p in serializedObject.targetObjects)
			{
				p.Randomize();
				azimuthProp.floatValue = p.Azimuth;
				altitudeProp.floatValue = p.Altitude;
				altitudeSnapProp.boolValue = p.AltitudeSnap;
				randomizedProp.boolValue = p.Randomized;
				randomAzimuthMinProp.floatValue = p.RandomAzimuthMin;
				randomAzimuthMaxProp.floatValue = p.RandomAzimuthMax;
				randomScaleMinProp.floatValue = p.RandomScaleMin;
				randomScaleMaxProp.floatValue = p.RandomScaleMax;
			}
		}
		
		GUILayout.Space(10);
		
		// Apply changes to edited object(s).
		serializedObject.ApplyModifiedProperties();
	}
	
	/*
	void OnSceneGUI () {
		SphereSnap p = (SphereSnap) target;
        p.Azimuth = Handles.Disc(p. (target.lookAtPoint, Quaternion.identity);
        if (GUI.changed)
            EditorUtility.SetDirty (target);
    }
    */
		
}
