using UnityEngine;
using System.Collections;

public class PlayerBlood : MonoBehaviour 
{
	
	public float MinBloodInterval = 0.25f;	
	
	void Start() 
	{
		ResetBlood();
	}
	
	private void OnPlayerDamaged(float damage)
	{
		particleSystem.enableEmission = true;
		PlayerController.Instance.Damaged -= OnPlayerDamaged;
		Invoke("ResetBlood", MinBloodInterval);
	}
	
	private void ResetBlood()
	{
		PlayerController.Instance.Damaged += OnPlayerDamaged;
		particleSystem.enableEmission = false;
	}
	
}
