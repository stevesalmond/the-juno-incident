using UnityEngine;
using System.Collections;

public class PlayerCameraEffects : MonoBehaviour {
	
	public float MinVignette = 1;
	public float MaxVignette = 25;
	
	public float MinCutoffFrequency = 100;
	public float MaxCutoffFrequency = 22000;
	
	private Vignetting vignette;
	private ColorCorrectionCurves curves;
	private AudioLowPassFilter audioMuffler;
	
	// Use this for initialization
	void Start () {
		vignette = GetComponent<Vignetting>();
		curves = GetComponent<ColorCorrectionCurves>();
		audioMuffler = GetComponent<AudioLowPassFilter>();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
		float t = PlayerController.Instance.HealthFactor;
		
		vignette.intensity = Mathf.Lerp(MinVignette, MaxVignette, 1 - t);
		
		curves.saturation = t;
		curves.enabled = t < 1;
		
		audioMuffler.cutoffFrequency = Mathf.Lerp(MinCutoffFrequency, MaxCutoffFrequency, t);
		audioMuffler.enabled = t < 1;
	}
}
