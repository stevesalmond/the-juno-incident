using UnityEngine;
using System.Collections;

public class PlayerBreathingEffort : MonoBehaviour 
{
	
	public float MinSpeed = 0.1f;
	
	public float MaxSpeed = 10f;
	
	// Update is called once per frame
	void Update()
	{
		// Get player's travel speed.
		PlayerController player = PlayerController.Instance;
		float speed = player.rigidbody.velocity.magnitude;
		float t = Mathf.Clamp((speed - MinSpeed) / (MaxSpeed - MinSpeed), 0, 1);
		audio.volume = t;
		audio.enabled = audio.volume > 0;
	}
	
}
