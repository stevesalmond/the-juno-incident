using UnityEngine;
using System.Collections;

public class PlayerFire : MonoBehaviour 
{

	// Properties
	// ----------------------------------------------------------------------------
	
	/** Input button bound to this fire type. */
	public string ButtonId = "Fire1";
	
	/** Object to emit projectiles from. */
	public GameObject Emitter;
	
	/** Object to shoot. */
	public GameObject Projectile;
	
	/** Offset to instantiate object at. */
	public Vector3 Offset;
	
	/** Initial velocity of object. */
	public Vector3 Velocity;
	
	/** Rate of fire. */
	public float FireRate = 1;
	
	/** Magnitude of recoil. */
	public float Recoil = 1;
	
	/** Scatter amount, in degrees */
	public float Scatter = 1;
	
	/** Whether to use pooled projectiles. */
	public bool Pooled = true;
	
	/** Firing sound. */
	public AudioClip FireSound;
	
	public float MinPitch = 1;
	public float MaxPitch = 1;
	public float MinTorque = 0;
	public float MaxTorque = 1;
	
	
	
	// Members
	// ----------------------------------------------------------------------------
	
	/** Time for next shot. */
	private float nextFire = 0;
	
	/** Rigidbody to apply recoil to. */
	private Rigidbody recoilBody;
	
	
	// Unity Implementation
	// ----------------------------------------------------------------------------
	
	/** Initialize the fire controller. */
	void Start()
	{
		// Locate a rigidbody to apply recoil to.
		recoilBody = FindRecoilRigidbody();
	}

	/** Update the fire controller. */
	void Update() 
	{
		// Check if player is alive.
		if (!PlayerController.Instance.IsAlive)
			return;
		
		// Check to see if we should fire.
		if (Input.GetButtonDown(ButtonId) && Time.time > nextFire) 
		{ 
			Fire();
			
			if (FireRate > 0)
				nextFire = Time.time + (1 / FireRate);
		}
		
	}
	
	
	// Private Methods
	// ----------------------------------------------------------------------------
	
	/** Fire a projectile. */
	private void Fire()
	{
		// Create the projectile.
		GameObject go;
		if (Pooled)
			go = ObjectPool.Instance.GetObject(Projectile, true);
		else
			go = Instantiate(Projectile) as GameObject;
		
		// Move it to initial location.
		go.transform.position = Emitter.transform.position;
		go.transform.rotation = Emitter.transform.rotation;
		go.transform.Translate(Offset, Space.Self);
		go.transform.Rotate(Random.onUnitSphere * Scatter);
		
		// Give it initial velocity and torque.
		go.rigidbody.velocity = go.transform.TransformDirection(Velocity);
		go.rigidbody.AddRelativeTorque(Random.onUnitSphere 
			* Random.Range(MinTorque, MaxTorque));
		
		// Add in recoil body's velocity.
		if (recoilBody != null)
			go.rigidbody.velocity += recoilBody.velocity;
		
		// Apply recoil.
		Vector3 recoil = go.rigidbody.velocity.normalized * -Recoil;
		if (recoilBody != null)
			recoilBody.AddForce(recoil);
		
		// Play firing sound.
		if (FireSound)
		{
			audio.pitch = Random.Range(MinPitch, MaxPitch);
			audio.PlayOneShot(FireSound);
		}
	}
	
	
	/** Locates a rigidbody to apply recoil to. */
	private Rigidbody FindRecoilRigidbody()
	{
		// Search through parents for a rigidbody.
		Transform t = transform;
		while (t != null && t.rigidbody == null)
			t = t.parent;
		
		// Return the located rigidbody.
		if (t != null)
			return t.rigidbody;
		else
			return null;
	}
	
}
