using UnityEngine;
using System.Collections;

public class PlayerController : Singleton<PlayerController> 
{
	
	// Delegates
	// ----------------------------------------------------------------------------
	
	public delegate void PlayerEvent();
	
	public delegate void CollisionEvent(Collision collision);
	
	public delegate void DamageEvent(float damage);
	
	
	// Events
	// ----------------------------------------------------------------------------
	
	/** Player jump event. */
	public event PlayerEvent Jumped;
	
	/** Player impacted on something. */
	public event CollisionEvent Collided;
	
	/** Player was damaged. */
	public event DamageEvent Damaged;
	
	/** Player died event. */
	public event PlayerEvent Died;
	
	
	// Properties
	// ----------------------------------------------------------------------------
	
	public float MoveSpeed;
	public float JumpSpeed;
	
	public bool Grounded
		{ get; private set; }
	
	public Transform Upright;
	
	public LayerMask GroundMask;
	
	public float GroundedThreshold = 0.25f;
	
	public string Name = "Player";
	
	public float Health
		{ get; private set; }
	
	public float HealthFactor
		{ get { return Health / 100; } } 
	
	public bool IsAlive
		{ get { return Health > 0; } }
	
	public bool IsDead
		{ get { return Health <= 0; } }
	
	public int Kills = 0;
	
	public float SurvivalTime
		{ get { return IsAlive ? Time.time - startTime : deathTime - startTime; } }
	
	public float DeathTime
		{ get { return IsDead ? Time.time - deathTime : 0; } }
	
	
	// Members
	// ----------------------------------------------------------------------------

	/** Local transform. */
	private Transform t;
	
	/** Timestamp player started playing. */
	private float startTime;
	
	/** Timestamp player died. */
	private float deathTime;
	
	
	// Unity Implementation
	// ----------------------------------------------------------------------------
	
	void Start()
	{
		t = transform;
		Health = 100;
		Name = NameProvider.Instance.Generate();
		startTime = Time.time;
		Application.targetFrameRate = 60;
	}
	
	/** Handle the  player colliding with something. */
	void OnCollisionEnter(Collision collision)
	{
		if (Collided != null)
			Collided(collision);
	}
	
	/** Update input from player. */
	void Update()
	{
		// Check if game is in play.
		if (IsAlive && Grounded)
			UpdateInput();
		
		// Go fullscreen on enter.
		if (Input.GetKeyDown(KeyCode.Return))
			Screen.fullScreen = !Screen.fullScreen;
	}

	/** Update the player's orientation. */
	void FixedUpdate() 
	{
		// Update player's orientation.
		Vector3 up = GetReferenceUp();
		Vector3 lookat = Upright.forward;
		Vector3 right = Vector3.Cross(up, lookat);
		Vector3 forward = Vector3.Cross(right, up);
		Upright.rotation = Quaternion.LookRotation(forward, up);
		
		// Update player's grounded status.
		Ray ray = new Ray(t.position, -t.position);
		Grounded = Physics.Raycast(ray, GroundedThreshold, GroundMask);
	}
	
	
	// Public Methods
	// ----------------------------------------------------------------------------
	
	/** Damage the player. */
	public float Damage(float damage)
	{
		// Can't be damaged when already dead.
		if (IsDead)
			return 0;
		
		// Apply damage to player.
		damage = Mathf.Min(damage, Health);
		Health -= damage;
		
		// Fire damage event.
		if (Damaged != null)
			Damaged(damage);
		
		// Check if player died as a result of damage.
		if (IsDead)
			Die();
		
		// Return applied damage.
		return damage;
	}
	
	
	// Private Methods
	// ----------------------------------------------------------------------------
	
	/** Updates player inputs. */
	private void UpdateInput()
	{
		// Framerate correction factor.
		float dt = Mathf.Clamp(Time.deltaTime * Application.targetFrameRate, 0.8f, 1.2f);
			
		// Get movement axes.
		Vector3 up = GetReferenceUp();
		Transform tCam = Camera.main.transform;
		Vector3 strafe = Vector3.Cross(up, tCam.forward);
		Vector3 move = Vector3.Cross(strafe, up);
		
		// Apply movement inputs.
		float dx = Input.GetAxis("Horizontal");
		float dy = Input.GetAxis("Vertical");
		Vector3 f = (move * dy + strafe * dx).normalized;
		rigidbody.AddForce(f * MoveSpeed * dt);
		
		// Apply jump.
		if (Input.GetButton("Jump"))
		{
			rigidbody.AddForce(up * JumpSpeed * dt);
			if (Jumped != null)
				Jumped();
		}
		
		// Apply slow-mo.
		if (Input.GetKeyDown(KeyCode.KeypadMinus))
			Time.timeScale *= 0.5f;
		else if (Input.GetKeyDown(KeyCode.KeypadPlus))
			Time.timeScale *= 2.0f;
	}
	
	private Vector3 GetReferenceUp()
		{ return t.position.normalized; }
	
	/** Handle the player dying. */
	private void Die()
	{
		// Record time of death.
		deathTime = Time.time;

		// Fire death event.
		if (Died != null)
			Died();
	}
	
	/** Reload the level. */
	public void Reload()
		{ Application.LoadLevel(Application.loadedLevel); }
	
}
