using UnityEngine;
using System.Collections;

public class PlayerJumpSounds : MonoBehaviour {
	
	public AudioClip[] JumpSounds;
	
	public float MinJumpInterval = 0.5f;
	
	void Start () {
		EnableJumpSound();
	}
	
	private void OnPlayerJumped()
	{
		Jump();
		
		PlayerController.Instance.Jumped -= OnPlayerJumped;
		Invoke("EnableJumpSound", MinJumpInterval);
	}
	
	private void EnableJumpSound()
	{
		PlayerController.Instance.Jumped += OnPlayerJumped;
	}
	
	
	/** Play a jump sound. */
	private void Jump()
	{
		int n = JumpSounds.Length;
		int i = Random.Range(0, n);
		AudioClip sound = JumpSounds[i];
		if (sound != null && gameObject.activeSelf)
			audio.PlayOneShot(sound);
	}
}
