using UnityEngine;
using System.Collections;

public class PlayerLightController : MonoBehaviour {
	
	
	public float BlinkDelay = 1;
	
	public Color BlinkColor = Color.green;
	
	public float BlinkTime = 0.25f;
	
	
	// Use this for initialization
	void Start () {
	
		Blink();
	}
	

	private void Blink()
	{
		Hashtable param = new Hashtable();
		param.Add("delay", BlinkDelay);
		param.Add("color", BlinkColor);
		param.Add("time", BlinkTime);
		param.Add("oncomplete", "Unblink");
		
		iTween.ColorTo(gameObject, param);
		
	}
	
	private void Unblink()
	{
		Hashtable param = new Hashtable();
		param.Add("color", Color.black);
		param.Add("time", BlinkTime);
		param.Add("oncomplete", "Blink");
		
		iTween.ColorTo(gameObject, param);
	}
}
