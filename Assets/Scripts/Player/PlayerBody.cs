using UnityEngine;
using System.Collections;

public class PlayerBody : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void LateUpdate () 
	{
		// Update body's orientation.
		Vector3 up = transform.position.normalized;
		Vector3 lookat = Camera.main.transform.forward;
		Vector3 right = Vector3.Cross(up, lookat);
		Vector3 forward = Vector3.Cross(right, up);
		transform.rotation = Quaternion.LookRotation(forward, up);
	}
}
