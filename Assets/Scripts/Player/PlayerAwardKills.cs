using UnityEngine;
using System.Collections;

public class PlayerAwardKills : MonoBehaviour {
	
	
	/** Number of kills to award. */
	public int Kills = 1;
	
	void Start () {
		PlayerController.Instance.Kills += Kills;
	}
}
