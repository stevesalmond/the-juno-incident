using UnityEngine;
using System.Collections;

public class PlayerCollideSounds : MonoBehaviour {
	
	public AudioClip[] CollideSounds;
	
	public LayerMask CollisionMask;
	
	
	public float MinForce = 10;
	
	public float MaxForce = 100;
	
	public float MinVolume = 0.2f;
	
	public float MaxVolume = 1.0f;
	
	public float MinInterval = 0.5f;
	
	
	void Start () {
		EnableCollisionSounds();
	}
	
	private void OnPlayerCollided(Collision collision)
	{
		// Check layer mask.
		int layer = collision.gameObject.layer;
		if ((CollisionMask.value & 1 << layer) == 0)
			return;
		
		// Check if force is sufficient to generate a sound.
		float f = collision.impactForceSum.magnitude;
		if (f < MinForce)
			return;
		
		// Play a collision sound.
		PlayCollisionSound(collision);
		
		// Disable collision sound for a bit..
		PlayerController.Instance.Collided -= OnPlayerCollided;
		Invoke("EnableCollisionSounds", MinInterval);
	}
	
	private void EnableCollisionSounds()
	{
		PlayerController.Instance.Collided += OnPlayerCollided;
	}
	
	
	/** Play a collision sound. */
	private void PlayCollisionSound(Collision collision)
	{
		// Determine sound volume based on collision force.
		float f = collision.impactForceSum.magnitude;
		float t = Mathf.Clamp((f - MinForce) / (MaxForce - MinForce), 0, 1);
		float volume = Mathf.Lerp(MinVolume, MaxVolume, t);
		
		// Play the sound.
		int n = CollideSounds.Length;
		int i = Random.Range(0, n);
		AudioClip sound = CollideSounds[i];
		if (sound != null && gameObject.activeSelf)
			audio.PlayOneShot(sound, volume);
	}
}
