using UnityEngine;
using System.Collections;

public class PlayerBreathingHurt : MonoBehaviour 
{
	
	public float MinHealthFactor = 0.0f;
	
	public float MaxHealthFactor = 0.9f;
	
	public float MinVolume = 0;
	
	public float MaxVolume = 1;
	
	public float MinPitch = 1;
	
	public float MaxPitch = 1;
	
	
	// Update is called once per frame
	void Update()
	{
		// Get player's healthiness.
		PlayerController player = PlayerController.Instance;
		float t = Mathf.Clamp((player.HealthFactor - MinHealthFactor) 
			/ (MaxHealthFactor - MinHealthFactor), 0, 1);
		
		
		audio.volume = Mathf.Lerp(MinVolume, MaxVolume, 1 - t);
		audio.pitch = Mathf.Lerp(MinPitch, MaxPitch, 1 - t);
		audio.enabled = audio.volume > 0;
	}
	
}
