using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;
using System;

public enum GUIState
{
	Playing,
	GameOver,
	Leaderboard
}

public class GameGUI : MonoBehaviour 
{
	
	// Properties
	// ----------------------------------------------------------------------------
	
	/** Skin to use for GUI elements. */
	public GUISkin Skin;
	
	
	// Members
	// ----------------------------------------------------------------------------
	
	/** The local player. */
	private PlayerController player;
	
	/** Large label style. */
	private GUIStyle largeLabelStyle;
	
	/** Medium label style. */
	private GUIStyle mediumLabelStyle;
	
	/** Small label style. */
	private GUIStyle smallLabelStyle;
		
	/** Reticule style. */
	private GUIStyle reticuleStyle;
	
	/** Current GUI state. */
	private GUIState state = GUIState.Playing;
	
	/** Scroll position for high scores view. */
	private Vector2 scoresScrollPosition = Vector2.zero;
	
	
	
	// Unity Implementation
	// ----------------------------------------------------------------------------
	
	/** Start the GUI. */
	void Start()
	{
		player = PlayerController.Instance;
		player.Died += OnPlayerDied;

		largeLabelStyle = Skin.customStyles[0];
		mediumLabelStyle = Skin.customStyles[1];
		smallLabelStyle = Skin.customStyles[2];
		reticuleStyle = Skin.customStyles[3];
	}
	
	/** Update the GUI. */
	void OnGUI()
	{	
		// Set up the GUI skin.
		GUI.skin = Skin;
		
		// Draw HUD.
		GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
		GUILayout.BeginVertical();
		OnGUIHeader();
	    GUILayout.FlexibleSpace();
		
		if (state == GUIState.GameOver)
			OnGUIGameOver();
		else if (state == GUIState.Leaderboard)
			OnGUILeaderboard();
		
		GUILayout.FlexibleSpace();
		
		if (state != GUIState.Leaderboard)
			OnGUIFooter();
		
		GUILayout.EndVertical();
		GUILayout.EndArea();
		
		if (state == GUIState.Playing)
			OnGUIReticule();
	}
	
	void OnGUIHeader()
	{
	}
		
	void OnGUIGameOver()
	{
		float alpha = Mathf.Clamp((float) player.DeathTime / 2f, 0, 1);
		GUI.color = new Color(1, 1, 1, alpha);
		
		GUILayout.BeginVertical();
		
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.Label("game over", largeLabelStyle);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		GUILayout.Space(15);
		
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		
		TimeSpan s = TimeSpan.FromSeconds(player.SurvivalTime);
		GUILayout.Label(string.Format("you survived for {0:D2}:{1:D2}.{2:D1}", 
			(int) s.TotalMinutes, s.Seconds, s.Milliseconds / 100), mediumLabelStyle);
		
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		
		GUILayout.Label(string.Format("and killed {0:D} zombonauts!", 
			player.Kills), mediumLabelStyle);
		
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		GUILayout.Space(15);
		
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		player.Name = GUILayout.TextField(player.Name, 20, GUILayout.Width(250));
		player.Name = Regex.Replace(player.Name, @"[^a-zA-Z0-9_]", "");
		if (GUILayout.Button("Submit Score"))
		{
			Leaderboard.Instance.AddScore(player.Name, player.Kills, (int) player.SurvivalTime);
			state = GUIState.Leaderboard;			
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		GUILayout.EndVertical();

		GUI.color = Color.white;
	}
	
	void OnGUILeaderboard()
	{
		GUILayout.BeginVertical();
		
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.Label("high scores", largeLabelStyle);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		GUILayout.Space(20);
		
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();

		Leaderboard.Score[] scores = Leaderboard.Instance.ToScoreArray();
		
		if (scores == null) 
			GUILayout.Label("loading...", mediumLabelStyle);
		else 
		{
			scoresScrollPosition = GUILayout.BeginScrollView(
				scoresScrollPosition, GUILayout.Width(800), GUILayout.Height(390));
			
			int rank = 1;
			
			foreach (Leaderboard.Score score in scores)
			{
				GUILayout.BeginHorizontal();
				GUILayout.Label(string.Format("{0:D}.", rank), smallLabelStyle, GUILayout.Width(100));
				GUILayout.Space(10);
				GUILayout.Label(score.playerName, smallLabelStyle);
				GUILayout.FlexibleSpace();
				GUILayout.Label(string.Format("{0:D}", score.score), smallLabelStyle);
				GUILayout.Space(20);
				TimeSpan s = TimeSpan.FromSeconds(score.seconds);
				GUILayout.Label(string.Format("{0:D2}:{1:D2}", 
					(int) s.TotalMinutes, s.Seconds), smallLabelStyle, GUILayout.Width(130));
				GUILayout.EndHorizontal();
				
				rank++;
			}
			
			GUILayout.EndScrollView();
		}
		
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		GUILayout.Space(20);
		
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		
		if (GUILayout.Button("Replay", GUILayout.Width(200)))
			player.Reload();
		
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		GUILayout.EndVertical();
	}
	
	void OnGUIFooter()
	{
		GUILayout.BeginHorizontal();
		GUILayout.Space(20);
		
		GUILayout.BeginVertical();
		GUILayout.Label("kills", mediumLabelStyle);
		GUILayout.Label(string.Format("{0:D5}", player.Kills), largeLabelStyle);
		GUILayout.EndVertical();
		
		GUILayout.Space(40);
		
		GUILayout.BeginVertical();
		GUILayout.Label("survival", mediumLabelStyle);
		TimeSpan s = TimeSpan.FromSeconds(player.SurvivalTime);
		GUILayout.Label(string.Format("{0:D2}:{1:D2}:{2:D1}", 
			(int) s.TotalMinutes, s.Seconds, s.Milliseconds / 100), largeLabelStyle);
		GUILayout.EndVertical();
		
		GUILayout.FlexibleSpace();
		
		GUILayout.BeginVertical();
		GUILayout.Label("health", mediumLabelStyle);
		GUILayout.Label(string.Format("{0:D3}", (int) player.Health), largeLabelStyle);
		GUILayout.EndVertical();
		
		GUILayout.Space(20);
		GUILayout.EndHorizontal();
	}
	
	void OnGUIReticule()
	{
		float sw = Screen.width;
		float sh = Screen.height;
		float cx = sw * 0.5f;
		float cy = sh * 0.5f;
		Rect r = new Rect(cx - 64, cy - 64, 128, 128); 
		GUI.Button(r, "", reticuleStyle);
	}
	
	
	void OnPlayerDied()
	{
		state = GUIState.GameOver;
	}

}
