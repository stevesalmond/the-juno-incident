using UnityEngine;
using System.Collections;

public class BrownianMotion : MonoBehaviour 
{
	
	// Properties
	// ------------------------------------------------------
	
	/** Overall wander force strength factor. */
	public float ForceStrength = 1;
	
	/** Minimum time between random force update. */
	public float ForceUpdateMin = 1;

	/** Minimum time between random force update. */
	public float ForceUpdateMax = 2;
	
	/** Minimum force vector (local space). */
	public Vector3 ForceMin = Vector3.zero;

	/** Maximum force vector (local space). */
	public Vector3 ForceMax = Vector3.zero;
	
	/** Force smoothing time. */
	public float ForceSmoothTime = 1;

	/** Overall wander torque strength factor. */
	public float TorqueStrength = 1;
	
	/** Minimum time between random force update. */
	public float TorqueUpdateMin = 1;

	/** Minimum time between random force update. */
	public float TorqueUpdateMax = 2;
	
	/** Minimum force vector (local space). */
	public Vector3 TorqueMin = Vector3.zero;

	/** Maximum force vector (local space). */
	public Vector3 TorqueMax = Vector3.zero;
	
	/** Torque smoothing time. */
	public float TorqueSmoothTime = 1;
	
	/** Object whose coordinate space we'll use when applying force. */
	public Transform RelativeTo;
	
	
	
	// Members
	// ------------------------------------------------------
	
	private float forceNextTime = 0;
	private Vector3 forceTarget;
	private Vector3 forceCurrent;
	private Vector3 forceVelocity;

	private float torqueNextTime = 0;
	private Vector3 torqueTarget;
	private Vector3 torqueCurrent;
	private Vector3 torqueVelocity;
	
	
	// Unity Implementation
	// ------------------------------------------------------
	
	/** Start this component. */
	void Start()
	{
		if (RelativeTo == null)
			RelativeTo = transform;
	}
	
	/** Update the wandering motion. */
	void FixedUpdate()
	{
		// Get current time.
		float t = Time.time;
		
		// Update the random force vector.
		if (t > forceNextTime)
		{
			// Generate new target force.
			float x = Random.Range(ForceMin.x, ForceMax.x);
			float y = Random.Range(ForceMin.y, ForceMax.y);
			float z = Random.Range(ForceMin.z, ForceMax.z);
			forceTarget = new Vector3(x, y, z) * ForceStrength;
			
			// Schedule next force update.
			forceNextTime = t + Random.Range(ForceUpdateMin, ForceUpdateMax);
		}
		
		// Update the random torque vector.
		if (t > torqueNextTime)
		{
			// Generate new target force.
			float x = Random.Range(TorqueMin.x, TorqueMax.x);
			float y = Random.Range(TorqueMin.y, TorqueMax.y);
			float z = Random.Range(TorqueMin.z, TorqueMax.z);
			torqueTarget = new Vector3(x, y, z) * TorqueStrength;
			
			// Schedule next force update.
			torqueNextTime = t + Random.Range(TorqueUpdateMin, TorqueUpdateMax);
		}
		
		// Update current force and torque towards target values.
		forceCurrent = Vector3.SmoothDamp(forceCurrent, forceTarget, ref forceVelocity, ForceSmoothTime);
		torqueCurrent = Vector3.SmoothDamp(torqueCurrent, torqueTarget, ref torqueVelocity, TorqueSmoothTime);
		
		// Compute worldspace force and torque.
		Vector3 forceWorld = RelativeTo.TransformDirection(forceCurrent);
		Vector3 torqueWorld = RelativeTo.TransformDirection(torqueCurrent);
		
		// Apply current force and torque.
		rigidbody.AddForce(forceWorld);
		rigidbody.AddTorque(torqueWorld);
	}
}
