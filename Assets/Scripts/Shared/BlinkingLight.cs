using UnityEngine;
using System.Collections;

public class BlinkingLight : MonoBehaviour {
	
	/** The component to enable/disable. */
	public Light Target;
	
	/** Cycle time. */
	public float Rate = 1;

	/** Initial delay. */
	public float InitialDelay = 0;
	
	// Use this for initialization
	void Start() 
		{ InvokeRepeating("Toggle", InitialDelay, Rate); }
	
	void Toggle()
		{ Target.enabled = !Target.enabled; }
	
	
	
}
