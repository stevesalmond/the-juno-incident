using UnityEngine;
using System.Collections;

public class FadeOut : MonoBehaviour {

	public float Duration = 0.5f;
	
	// Use this for initialization
	void Start () {
		iTween.ColorTo(gameObject, Color.black, Duration);
	}
}
