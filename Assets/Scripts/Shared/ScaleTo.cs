using UnityEngine;
using System.Collections;

public class ScaleTo : MonoBehaviour {

	public float Duration = 0.5f;
	
	public Vector3 Scale = Vector3.zero; 
	
	// Use this for initialization
	void Start () {
		iTween.ScaleTo(gameObject, Scale, Duration);
	}
}
