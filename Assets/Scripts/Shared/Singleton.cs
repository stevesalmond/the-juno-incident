using UnityEngine;
using System.Collections;

/** 
 * Convenience base class for creating Singleton behaviours.
 * 
 * Taken from: http://devmag.org.za/2012/07/12/50-tips-for-working-with-unity-best-practices/
 * 
 * Note: Avoid using singletons for unique instances of prefabs 
 * that are not managers (such as the Player). Not adhering to 
 * this principle complicates inheritance hierarchies, and makes 
 * certain types of changes harder. Rather keep references to 
 * these in your GameManager (or other suitable God class)
 */ 
public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
   protected static T instance;
 
   /** Returns the instance of this singleton. */
   public static T Instance
   {
      get
      {
         if(instance == null)
         {
            instance = (T) FindObjectOfType(typeof(T));
 
            if (instance == null)
            {
               Debug.LogError("An instance of " + typeof(T) + 
                  " is needed in the scene, but there is none.");
            }
         }
 
         return instance;
      }
   }
	
   /** Returns whether an instance of this singleton exists in the scene. */
   public static bool HasInstance
   {
      get { return FindObjectOfType(typeof(T)) != null; }
   }
	
	
   /** Returns whether an instance of this singleton exists in the scene. */
   public static bool Instantiated
   {
      get { return instance != null; }
   }

}
