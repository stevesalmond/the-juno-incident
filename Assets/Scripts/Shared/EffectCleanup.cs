using UnityEngine;
using System.Collections;

/** Cleans up (destroys) completed particle effects. */
public class EffectCleanup : MonoBehaviour {
	
	
	// Properties
	// ----------------------------------------------------------------------------

	/** Whether effect is pooled. */
	public bool Pooled = false;
	
	/** Minimum lifetime. */
	public float MinLifetime = 0;
	
	
	// Members
	// ----------------------------------------------------------------------------

	/** Cached particle system component. */
	private ParticleSystem particles;
	
	/** Time when effect was started. */
	private float startTime;
	
	
	// Unity Implementation
	// ----------------------------------------------------------------------------
	
	/** Initializes the cleanup script. */
	void Start () {
		particles = particleSystem;
		startTime = Time.time; 
	}
	
	/** Updates the cleanup script. */
	void Update () {
		
		if (audio && audio.isPlaying)
			return;
		if (particles && particles.IsAlive())
			return;
		if (Time.time - startTime < MinLifetime)
			return;
		
		if (Pooled)
			ObjectPool.Instance.ReturnObject(gameObject);
		else
			Destroy(gameObject);
	}
	
}
