using UnityEngine;
using System.Collections;

public class SnapTo : MonoBehaviour 
{
	
	public GameObject Target;

	void LateUpdate() 
	{
		transform.position = Target.transform.position;
	}
	
}
