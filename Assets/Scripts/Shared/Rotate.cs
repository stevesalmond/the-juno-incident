using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour 
{
	
	/** Axis of rotation. */
	public Vector3 Axis = new Vector3(1, 0, 0);
	
	/** Speed of rotation. */
	public float Speed = 0.05f;
	

	/** Update rotation. */
	void Update () 
	{
		float dTheta = Speed * Time.deltaTime;
		transform.RotateAround(Axis, dTheta);
	}
}
