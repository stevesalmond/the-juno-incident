using UnityEngine;
using System.Collections;

public class NameProvider : Singleton<NameProvider> {
	
	public string[] Fragments;
	
	public string Generate()
	{
		string name = "";
		
		for (int i=0; i<3; i++)
			name += GetRandomNameFragment();
		
		return name;
	}
	
	private string GetRandomNameFragment()
	{
		int n = Fragments.Length;
		int i = Random.Range(0, n);
		return Fragments[i];
	}
	
}
