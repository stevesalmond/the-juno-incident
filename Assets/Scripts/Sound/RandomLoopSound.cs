using UnityEngine;
using System.Collections;

public class RandomLoopSound : MonoBehaviour {

	public AudioClip[] Sounds;
	public float MinPitch = 1;
	public float MaxPitch = 1;
	
	// Use this for initialization
	void Start () {
		audio.clip = Sounds[Random.Range(0, Sounds.Length)];
		audio.pitch = Random.Range(MinPitch, MaxPitch);
		audio.Play();
	}
	
}
