using UnityEngine;
using System.Collections;

/** Plays a random sound when the object becomes active. */
public class RandomEnableSound : RandomSound
{
	
	// Unity Implementation
	// ----------------------------------------------------------------------------

	/** Handle the game object becoming active. */
	public void OnEnable() 
	{
		// Play a random sound.
		Play();
	}
}
