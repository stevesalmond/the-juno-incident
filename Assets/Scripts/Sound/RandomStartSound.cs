using UnityEngine;
using System.Collections;

/** Plays a random sound when the behavior starts. */
public class RandomStartSound : RandomSound
{
	// Unity Implementation
	// ----------------------------------------------------------------------------

	/** Initializes the component. */
	public void Start () 
	{
		// Play a random sound.
		Play();
	}
}
