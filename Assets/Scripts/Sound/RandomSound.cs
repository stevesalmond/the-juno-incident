using UnityEngine;
using System.Collections;

/** Plays a random sound. */
public class RandomSound : MonoBehaviour 
{
	// Properties
	// ----------------------------------------------------------------------------
	
	/** Probability of playing the sound. */
	public float Probability = 1;

	/** The set of possible sounds. */
	public AudioClip[] Sounds;
	
	/** Minimum sound volume. */
	public float MinVolume = 0.5f;
	
	/** Maximum sound volume. */
	public float MaxVolume = 1.0f;
	
	/** Minimum pitch (relative to normal). */
	public float MinPitch = 1.0f;
	
	/** Maximum pitch (relative to normal). */
	public float MaxPitch = 1.0f;
	
	/** Minimum delay. */
	public float MinDelay = 0;
	
	/** Maximum delay. */
	public float MaxDelay = 0;
	
	/** True to allow the same sounds to be played twice. */
	public bool AllowSameSequential = false;
	
	
	// Properties
	// ----------------------------------------------------------------------------
	
	/** The index of the last sound. */
	private int lastIndex = -1;
	
	/** Audio source to use. */
	private AudioSource source;

	
	// Public Interface
	// ----------------------------------------------------------------------------

	/** Play a random sound. */
	public void Play(AudioSource source = null)
	{
		// Check if we should play a sound this time.
		if (Random.value > Probability)
			return;
		
		// Use own audio source if not supplied.
		if (source == null)
			this.source = audio;
		else
			this.source = source;
		
		// Play immediately, or after a delay.
		if (MaxDelay == 0)
			PlayImmediate();
		else
			Invoke("PlayImmediate", Random.Range(MinDelay, MaxDelay));
	}
	
	/** Actually plays a random sound. */
	private void PlayImmediate()
	{
		// Get a random audio clip.
		AudioClip clip = GetRandomClip();
		
		// Play the sound.
		if (source && clip) 
		{
			float volume = Random.Range(MinVolume, MaxVolume);
			source.pitch = Random.Range(MinPitch, MaxPitch);
			source.PlayOneShot(clip, volume);
		}
	}
	
	/** Get a random clip. */
	public AudioClip GetRandomClip()
	{
		// Get a random index into the sound array.
		int i = Random.Range(0, Sounds.Length);
		
		// Use the next sound if the same sound is selected again.
		if (!AllowSameSequential && i == lastIndex)
		{
			i++;
			if (i >= Sounds.Length)
				i = 0;
		}
		
		// Record the last index.
		lastIndex = i;
		
		// Check that sound index is ok.
		if (i < 0 || i >= Sounds.Length)
			return null;
		else
			return Sounds[i];
	}
}
