using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/** The object pool maintains lists of available
  * objects separated by type, and provides methods
  * for getting and recycling them. */ 
public class ObjectPool : Singleton<ObjectPool>
{
	// Members
	// ----------------------------------------------------------------------------

	/** Lists of objects mapped to object types. */
	private Dictionary<string, Stack<GameObject>> objectLists;
	
	
	// Life Cycle
	// ----------------------------------------------------------------------------
	
	/** Constructor. */
	public ObjectPool()
	{
		// Create the object list map.
		objectLists = new Dictionary<string, Stack<GameObject>>();
	}

	
	// Public Interface
	// ----------------------------------------------------------------------------
	
	/** Get a object of the specified type. */
	public GameObject GetObject(Object type, bool active = true)
	{
		// Get the list of available objects.
		Stack<GameObject> objects = GetObjectList(type);
		
		// Instantiate a new object if there are none in the list.
		if (objects.Count <= 0)
		{
			GameObject o = (GameObject)Instantiate(type);
			o.name = type.name;
			
			if (!active)
				o.SetActive(false);
			
			return o;
		}
		
		// Otherwise, remove and return the last object.
		else
		{
			GameObject o = objects.Pop();
			
			if (active)
				o.SetActive(true);
	
			return o;
		}
	}
	
	/** Return a object to the pool. */
	public void ReturnObject(GameObject o, bool unparent = false)
	{
		// Check that the object exists.
		if (o == null)
			return;
		
		// Deactivate the object.
		o.SetActive(false);
		
		// Unparent.
		if (unparent)
			o.transform.parent = null;
		
		// Get the list of objects for the object type.
		Stack<GameObject> objects = GetObjectList(o);
		
		// Add the object back in to the list.
		objects.Push(o);
	}
	
	/** Log a report of pooled objects. */
	public void Report()
	{
		string report = "";
		
		foreach (KeyValuePair<string, Stack<GameObject>> pair in objectLists)
		{
			Stack<GameObject> objects = pair.Value;
			
			if (objects.Count <= 0)
				continue;
			
			report += objects.Peek().name + " " + objects.Count + "\n";
		}
		
		Debug.Log(report);
	}
	
	
	// Private Methods
	// ----------------------------------------------------------------------------

	/** Get a list of avaialable objects for the specified object type. */
	private Stack<GameObject> GetObjectList(Object type)
	{
		// The map key is the name of the type.
		string key = type.name;
		
		// The list of objects.
		Stack<GameObject> objects;
		
		// Create a new object list if it doesn't exist yet.
		if (!objectLists.ContainsKey(key))
		{
			objects = new Stack<GameObject>();
			objectLists.Add(key, objects);
		}
		
		// Otherwise, find the mapped object list.
		else
			objects = objectLists[key];
		
		return objects;
	}
}
