using UnityEngine;
using System.Collections;

/** Applies a constant gravity force towards the origin. */
public class Gravity : MonoBehaviour
{
	
	// Properties
	// ----------------------------------------------------------------------------
	
	/** Gravity force magnitude. */
	public float Acceleration = 3;

	/** Minimum speed at which to apply gravity. */
	public float SpeedThreshold = 0.0f;
	
	/** Speed at which to put the object to sleep. */
	public float SleepSpeed = 0.0f;
	
	/** Whether to sleep at startup. */
	public bool SleepOnStart = false;
	
	/** Time taken to allow the level to 'warm up' */
	public float WarmupTime = 2.0f;
	
	
	// Members
	// ----------------------------------------------------------------------------

	/** Local transform. */
	protected Transform t;
	
	
	// Unity Implementation
	// ----------------------------------------------------------------------------
	
	/** Initializes the component. */
	public void Start()
	{
		// Cache transform for efficient access.
		t = transform;
	}
	
	/** Update gravity during physics phase. */
	void FixedUpdate()
	{
		// Check if physics is suspended.
		if (rigidbody.IsSleeping())
			return;
		
		// Don't apply gravity until the level has 'warmed up'.
		if (SleepOnStart && Time.time < WarmupTime)
		{ 
			Sleep();
			return;
		}

		// Check if we should apply gravity.
		float v = rigidbody.velocity.magnitude;

		// Check if we should put the object to sleep.
		if (v < SleepSpeed)
		{ 
			Sleep();
			return;
		}
		
		// Apply gravity to the object.
		if (v >= SpeedThreshold)
			Apply();
	}
	
	/** Applies gravity to the object for one physics update. */
	protected virtual void Apply()
	{
		float f = rigidbody.mass * Acceleration;
		rigidbody.AddForce((-t.position).normalized * f);
	}
	
	/** Put the object to sleep. */
	private void Sleep()
	{
		rigidbody.velocity = Vector3.zero;
		rigidbody.angularVelocity = Vector3.zero;
		rigidbody.Sleep();
	}
	
}
