using UnityEngine;
using System.Collections;

/** Orients an object correctly on a spherical planet surface (edit mode only). */
[ExecuteInEditMode]
public class SphereSnap : MonoBehaviour
{
	
	// Constants
	// ----------------------------------------------------------------------------

	/** Default planet radius to snap to. */
	public const float DefaultSnapRadius = 5.0f;
	
	
	// Properties
	// ----------------------------------------------------------------------------
	
	/** Rotation of the object about its 'up' normal. */
	public float Azimuth = 0.0f;

	/** Offset from the planet surface. */
	public float Altitude = 0.0f;

	/** Whether altitude snapping is allowed for this object. */
	public bool AltitudeSnap = false;
	
	/** Whether randomization is allowed for this object. */
	public bool Randomized = true;
	
	/** Min azimuth variation for the object. */
	public float RandomAzimuthMin = 0;

	/** Max azimuth variation for the object. */
	public float RandomAzimuthMax = 360;

	/** Min scale variation for the object. */
	public float RandomScaleMin = 1;

	/** Max scale variation for the object. */
	public float RandomScaleMax = 1;

	
	// Members
	// ----------------------------------------------------------------------------
	
	/** Cached transform. */
	private Transform t;
	
	/** Rotation angles to place object upright on planet. */
	private Vector3 angles = new Vector3(270, 0, 0);
	
	
	
	// Unity Implementation
	// ----------------------------------------------------------------------------
	
	/** Starts the snap component. */
	public void Start()
	{
		// Cache the transform.
		t = transform;
		
		// Disable when playing the game.
		if (Application.isPlaying)
			enabled = false;
	}
	
	/** Snaps the object to orient correctly with respect to the planet. */
	public void Update()
	{
		// Make sure transform is cached.
		if (!t)
			t = transform;
		
		// Get the correct radius to snap to.
		float radius = GetSnapRadius();
		
		// Snap to correct altitude above planet.
		if (AltitudeSnap)
			t.position = t.position.normalized * (radius + Altitude);
		
		// Orient to face away from the planet's center.
		// Then, rotate object Azimuth degrees about the normal.
		t.LookAt(Vector3.zero);
		t.Rotate(angles);
		t.Rotate(0, Azimuth, 0);
		
		// Bring rotation back into a sensible value range.
		Vector3 euler = t.rotation.eulerAngles;
		euler.x = CorrectedAngle(euler.x);
		euler.y = CorrectedAngle(euler.y);
		euler.z = CorrectedAngle(euler.z);
		
		// Debug.Log("Corrected rotation: " + euler);
		
		t.eulerAngles = euler;
	}
	
	private float CorrectedAngle(float value)
	{
		float corrected = value % 360;
		if (corrected < 0)
			corrected += 360;
		return corrected;
	}
	
	// Public Interface
	// ----------------------------------------------------------------------------

	/** Initializes the snapping behavior. */
	public void Randomize()
	{
		// Check if this object should be randomized.
		if (!Randomized)
			return;
		
		// If so, adjust its properties.
		Azimuth = Random.Range(RandomAzimuthMin, RandomAzimuthMax);
		float scale = Random.Range(RandomScaleMin, RandomScaleMax);
		transform.localScale = new Vector3(scale, scale, scale);
	}
	
	/** Returns the current snapping radius for the object. */
	private float GetSnapRadius()
	{
		// Check for a parent.
		if (!t.parent)
			return DefaultSnapRadius;
		
		// Check for a parent snap surface.
		SphereSnapSurface surface = t.parent.GetComponent<SphereSnapSurface>();
		if (surface)
			return surface.SnapRadius;
		
		// Use default snap radius.
		return DefaultSnapRadius;
	}
	
}
