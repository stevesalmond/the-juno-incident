using UnityEngine;
using System.Collections;

public class SphereSnapSurface : MonoBehaviour {
	
	/** The snapping radius for children in this group. */
	public float SnapRadius = 5.0f;
	
}
