using UnityEngine;
using System.Collections;

/** Culls objects when they go 'behind' the planet from the camera's POV. */

public class SphereCull : MonoBehaviour 
{
	
	// Properties
	// ----------------------------------------------------------------------------
	
	/** Angle at which to hide object (between camera and object positions). */
	public float AngularThreshold = 100.0f;
	
	
	// Members
	// ----------------------------------------------------------------------------
	
	/** Cached transform. */
	private Transform t;

	/** Cached camera transform. */
	private Transform tCamera;

	/** Whether object is currently culled. */
	private bool culled = false;
	
	
	// Unity Implementation
	// ----------------------------------------------------------------------------

	/** Initialize culling. */
	public void Start() 
	{
		t = transform;
		tCamera = Camera.main.transform;	
	}
	
	/** Update culling state. */
	public void Update() 
	{
		UpdateCulling();
	}
	
	// Private Methods
	// ----------------------------------------------------------------------------
	
	/** Update culling state. */
	private void UpdateCulling()
	{
		// Check the angle between the camera an object.
		// If the angle is sufficiently high, that means the object
		// must be 'behind' the planet and can be culled this frame.
		float angle = Vector3.Angle(t.position, tCamera.position);
		
		// Cull or restore the object as needed.
		if (angle < AngularThreshold)
			Uncull();
		else
			Cull();
	}

	/** Culls the object from scene. */
	private void Cull()
	{
		// Check if we've already culled the object.
		if (culled)
			return;
		
		// Mark object as culled.
		culled = true;
		
		// Disable each child object's renderer.
		Renderer[] rs = GetComponentsInChildren<Renderer>();
		foreach (Renderer r in rs)
			r.enabled = false;
	}

	/** Restores the object into the scene. */
	private void Uncull()
	{
		// Check if we've already restored the object.
		if (!culled)
			return;
		
		// Mark object as restored.
		culled = false;
		
		// Restore each child object's renderer.
		Renderer[] rs = GetComponentsInChildren<Renderer>();
		foreach (Renderer r in rs)
			r.enabled = true;
	}
	
}
