using UnityEngine;
using System.Collections;

public class SurfaceOrient : MonoBehaviour 
{
	
	public Transform Upright;
	
	public float FacingSmoothTime = 1;
	public float FacingVelocityThreshold = 0.5f;
	
	private Vector3 lookatCurrent;
	private Vector3 lookatVelocity;
	
	
	void Start()
	{
		lookatCurrent = Upright.forward;
	}
	
	/** Update the player's orientation. */
	void FixedUpdate() 
	{
		// Update objects's orientation.
		Vector3 up = GetReferenceUp();
		Vector3 lookatTarget = rigidbody.velocity;
		if (lookatTarget.magnitude < FacingVelocityThreshold)
			lookatTarget = Upright.forward;
		
		lookatCurrent = Vector3.SmoothDamp(lookatCurrent, lookatTarget, ref lookatVelocity, FacingSmoothTime);
		
		Vector3 right = Vector3.Cross(up, lookatCurrent);
		Vector3 forward = Vector3.Cross(right, up);
		Upright.rotation = Quaternion.LookRotation(forward, up);
	}
	
	private Vector3 GetReferenceUp()
		{ return Upright.position.normalized; }
}
