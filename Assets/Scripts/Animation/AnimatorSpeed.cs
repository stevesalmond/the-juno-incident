using UnityEngine;
using System.Collections;

public class AnimatorSpeed : MonoBehaviour {
	
	public Rigidbody Rigidbody;
	
	private Animator animator;
	
	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		float speed = Rigidbody.velocity.magnitude;
		animator.SetFloat("Speed", speed);
	}
}
