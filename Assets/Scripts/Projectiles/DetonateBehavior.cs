using UnityEngine;
using System.Collections;

public class DetonateBehavior : MonoBehaviour 
{
	
	// Properties
	// ----------------------------------------------------------------------------

	/** Destruction effect. */
	public GameObject DetonateEffect;
	
	/** Location to place detonation. */
	public Transform DetonateLocation;
	
	/** Whether object is pooled. */
	public bool Pooled = false;
	
	/** Blast radius. */
	public float BlastRadius = 2;
	
	/** Minimum damage amount. */
	public float MinDamage = 1;
	
	/** Maximum damage amount. */
	public float MaxDamage = 10;
	
	
	void Start()
	{
		if (DetonateLocation == null)
			DetonateLocation = transform;
	}
	
	/** Detonate the object. */
	public void Detonate(Vector3 position, Quaternion rotation, Vector3 direction)
	{
		// Apply blast radius effect.
		ApplyBlast(transform.position, BlastRadius);
		
		// Create the detonation effect.
		if (DetonateEffect != null && DetonateLocation != null)
		{
			GameObject go = ObjectPool.Instance.GetObject(DetonateEffect, true);
			go.transform.position = DetonateLocation.position;
			go.transform.rotation = DetonateLocation.rotation;
			
			// Try to configure detonator.
			Detonator det = go.GetComponent<Detonator>();
			if (det != null)
				det.direction = direction;
		}
		
		// Destroy this object.
		if (Pooled)
			ObjectPool.Instance.ReturnObject(gameObject, false);
		else
			Destroy(gameObject);
	}
	
	
	/** Damage everything in the blast radius. */
	private void ApplyBlast(Vector3 location, float radius) 
	{
	    Collider[] colliders = Physics.OverlapSphere(location, radius);
	    foreach (Collider hit in colliders) 
		{
			// Try to turn affected object into a ragdoll.
			Ragdollify r = hit.GetComponent<Ragdollify>();
			if (r != null)
				r.Apply();
			
			// Damage player if in blast radius.
			PlayerController player = hit.GetComponent<PlayerController>();
			if (player != null)
			{
				float t = 1 - Vector3.Distance(location, hit.transform.position) / radius;
				float damage = Mathf.Lerp(MinDamage, MaxDamage, t);
				player.Damage(damage);
			}
				
		}
	}
	
	
}
