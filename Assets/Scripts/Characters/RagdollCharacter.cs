using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/** A ragdoll representation of a character. */
public class RagdollCharacter : MonoBehaviour
{
	// Properties
	// ----------------------------------------------------------------------------	
	
	/** The skeleton root rigid body. */
	public Rigidbody Root;

	
	// Members
	// ----------------------------------------------------------------------------	
	
	/** All transform in the ragdoll, mapped to names. */
	Dictionary<string, Transform> transformMap;
	
	/** All rigidbodies in the ragdoll. */
	Rigidbody[] rigidbodies;

	
	
	// Unity Implementation
	// ----------------------------------------------------------------------------	
	
	/** Preinitialize. */
	void Awake()
	{
		// Populate the transform map.
		PopulateTransforms();
		
		// Get rigidbodies.
		rigidbodies = GetComponentsInChildren<Rigidbody>();
	}
	
	
	// Public Interface
	// ----------------------------------------------------------------------------	
	
	/** Reset. */
	public void Reset()
	{
		// Reset rigidbody velocities.
		foreach (Rigidbody body in rigidbodies)
		{
			body.velocity = Vector3.zero;
			body.angularVelocity  = Vector3.zero;
	        body.isKinematic = true;
	        body.isKinematic = false;			
		}
	}
	
	/** Initialize ragdoll from the source object. */
	public void SetTransforms(GameObject source)
	{
		// Set top level transform.
		transform.localPosition = source.transform.position;
		transform.localRotation = source.transform.rotation;
		// transform.localScale = source.transform.localScale;
		
		// Get a list of transform from the source object.
		Transform[] sourceTransforms = source.GetComponentsInChildren<Transform>();
		
		// Process each transform.
		foreach (Transform s in sourceTransforms)
		{
			// Check if the ragdoll has a corresponding transform.
			if (!transformMap.ContainsKey(s.name))
				continue;
			
			// Get the corresponding ragdoll transform.
			Transform t = transformMap[s.name];
			
			// Set the ragdoll transform to match the source transform.
			t.localRotation = s.localRotation;
			t.localPosition = s.localPosition;
			t.localScale = s.localScale;
		}
	}
	
	/** Initialize ragdoll velocities from the source rigidbody. */
	public void SetVelocities(GameObject source)
	{	
		// Inherit source rigidbody's motion.
		if (source.rigidbody)
			foreach (Rigidbody body in rigidbodies)
				body.velocity = source.rigidbody.velocity;
	}

	
	// Private Methods
	// ----------------------------------------------------------------------------	
	
	/** Populate the transform map. */
	private void PopulateTransforms()
	{
		// Create the transform map.
		transformMap = new Dictionary<string, Transform>();
		
		// Get a list of all transforms.
		Transform[] transforms = Root.GetComponentsInChildren<Transform>();
	
		// Map each transform to its name.
		foreach (Transform t in transforms)
		{
			// Ignore transforms with the
			// same name as one already mapped.
			if (transformMap.ContainsKey(t.name))
				continue;

			transformMap.Add(t.name, t);
		}
	}
}
