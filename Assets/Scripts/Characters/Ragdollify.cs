using UnityEngine;
using System.Collections;

public class Ragdollify : MonoBehaviour {
	
	/** The ragdoll to create.*/
	public GameObject Ragdoll;
	
	/** Location to create ragdoll at. */
	public GameObject Source;
	
	/** Whether effect has been applied./ */
	private bool applied = false;
	
	void Start()
	{
		if (!Source)
			Source = gameObject;
	}

	/** Turn this object into a ragdoll. */
	public void Apply()
	{
		// Check if we've already ragdolled this character.
		if (applied)
			return;
		
		// Create the ragdoll.
		Ragdoll = Instantiate(Ragdoll) as GameObject;
		
		// Initialize the ragdoll character.
		RagdollCharacter character = Ragdoll.GetComponent<RagdollCharacter>();
		if (character != null)
		{
			character.SetTransforms(Source);
			character.SetVelocities(gameObject);
		}
		
		// Destroy this object.
		Destroy(gameObject);
		
		// Ragdoll has been applied.
		applied = true;
	}
}
