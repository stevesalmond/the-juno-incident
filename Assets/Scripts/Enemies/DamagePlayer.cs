using UnityEngine;
using System.Collections;

public class DamagePlayer : MonoBehaviour 
{
	
	/** Base proximity damage. */
	public float Damage = 10;
	
	/** Proximity range. */
	public float BlastRadius = 2;
	
	/** Proximity explosive force. */
	public float BlastForce = 100;
	
	/** Minimum time between hits. */
	public float ReloadMinTime = 0.75f;

	/** Maximum time between hits. */
	public float ReloadMaxTime = 1.25f;
	
	/** Damage hit effect. */
	public GameObject DamageEffect;
	
	/** Animator to set attacking parameter on. */
	public Animator Animator;
		
	
	private Transform t;
	
	
	void Start () 
	{
		
		t = transform;

		// Periodically hit things.
		ScheduleHit();
	}
	
	private void ScheduleHit() 
	{
		Invoke("ApplyProximityDamage", 
			Random.Range(ReloadMinTime, ReloadMaxTime));
	}
	
	private void ApplyProximityDamage() 
	{
		// Apply proximity damage.
		DamageArea(t.position, BlastRadius, Damage);
		
		// Schedule next hit.
		ScheduleHit();
	}
	
	private void DamageArea(Vector3 location, float radius, float damage) 
	{
	    Collider[] colliders = Physics.OverlapSphere(location, radius);
	    foreach (Collider hit in colliders)
			if (Hurt(hit.gameObject, damage)) 
			{
				// Apply explosion force.
				if (hit.rigidbody)
		        	hit.rigidbody.AddExplosionForce(
						BlastForce, location, radius);
			
				// Schedule animation reset.
				Invoke("ResetAnimator", 0.1f);
			
				return;
			}
	}
	
	private void ResetAnimator()
	{
		// Reset attack on animator.
		if (Animator)
			Animator.SetBool("Attacking", false);
	}
	
	private bool Hurt(GameObject target, float damage) 
	{
		
		if (!target)
			return false;
		if (target.tag != "Player")
			return false;
		
		float damageGiven = PlayerController.Instance.Damage(damage);
		
		if (Animator)
			Animator.SetBool("Attacking", true);
			
		if (DamageEffect)
			Instantiate(DamageEffect, 
				target.transform.position, 
				target.transform.rotation);
		
		return (damageGiven > 0);
	}
}
