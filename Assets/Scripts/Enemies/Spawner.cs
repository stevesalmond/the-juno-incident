using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{

	/** The type of object to spawn. */
	public GameObject SpawnedObject;
	
	/** Spawn radius. */
	public float Radius = 55;
	
	
	/** Minimum spawn interval. */
	public float SpawnRateInitial = 0.1f;
	
	/** Maximum spawn interval. */
	public float SpawnRateFinal = 1f;
	
	/** Spawn variance percentage. */
	public float SpawnVarianceFraction = 0.2f;
	
	/** Time taken for spawn to reach maximum rate.. */
	public float SpawnRampTime = 180f;
	
	
	
	private float startTime = 0;
	
		
	// Use this for initialization
	void Start () 
	{
		startTime = Time.time;
		
		// Perform initial spawn.
		Spawn();
	}

	
	/** Spawn an object. */
	private void Spawn()
	{
		// Check if player is alive.
		if (!PlayerController.Instance.IsAlive)
			return;
		
		// Spawn an object.
		GameObject go = Instantiate(SpawnedObject) as GameObject;
		go.transform.position = Random.onUnitSphere * Radius;
		
		// Determine when next spawn should be.
		float elapsed = Time.time - startTime;
		float t = Mathf.Clamp(elapsed / SpawnRampTime, 0, 1);
		float rate = Mathf.Lerp(SpawnRateInitial, SpawnRateFinal, t);
		float duration = (1 / rate);
		float variance = Random.Range(-0.5f, 0.5f) * SpawnVarianceFraction;
		float next =  duration * (1 + variance);
		
		// Schedule next spawn.
		Invoke("Spawn", next);
	}
	
	
}
