using UnityEngine;
using System.Collections;

public class Seek : MonoBehaviour {
	
	/** How aggressively we seek the target. */
	public float Magnitude = 10.0f;
	
	/** Seek target distance. */
	public float SeekDistance = 0;
	
	/** How often we check for forward progress. */
	private float ProgressCheckTime = 5;

	/** Threshold distance for forward progress. */
	private float ProgressThreshold = 0.5f;
	
	/** Threshold distance no longer checking. */
	private float ProgressCutoff = 4;

	/** Minimum delay for resetting progress. */
	private float ProgressResetMinTime = 5;
	
	/** Maximum delay for resetting progress. */
	private float ProgressResetMaxTime = 10;
	
	/** The current target. */
	public GameObject Target;
	
	/** Last known target. */
	private GameObject LastTarget;
	
	/** Cached transform reference. */
	private Transform t;
	
	/** Distance to target at last progress check. */
	private float ProgressCheckpoint = float.MaxValue;
	
	/** Are we currently seeking the target? */
	private bool Seeking = true;
	
	
	// Use this for initialization
	protected virtual void Start () {
		t = transform;
		
		// Check progress periodically.
		InvokeRepeating("ProgressCheck", 
			ProgressCheckTime, 
			ProgressCheckTime);
	}
	
	void FixedUpdate() {
		
		// Are we seeking?
		if (!Seeking)
			return;
		
		// Check that we have a target.
		if (!Target)
			return;
		
		// Reset progress if target changed.
		if (Target != LastTarget)
			ResetProgress();
		
		// Remember last target.
		LastTarget = Target;
		
		// Check if we need to seek closer.
		if (TargetDistance() < SeekDistance) {
			ResetProgress();
			return;
		}
		
		// Generate impulse towards the target.
		Vector3 d = (Target.transform.position - t.position).normalized;
		Vector3 up = t.position.normalized;
		Vector3 side = Vector3.Cross(up, d);
		Vector3 f = Vector3.Cross(side, up);
		
		// Move towards the target.
		rigidbody.AddForce(f * Magnitude);
	}
	
	private void ProgressCheck() {
		
		// Check that we have a target.
		if (!Target)
			return;
		
		// Measure distance to target.
		float d = Vector3.Distance(t.position, Target.transform.position);
		
		// Are we closer than cutoff distance?
		if (d < ProgressCutoff)
			return;
		
		// Check progress since last checkpoint.
		bool wasSeeking = Seeking;
		if (ProgressCheckpoint < float.MaxValue) {
			float progress = (ProgressCheckpoint - d);
			Seeking = (progress >= ProgressThreshold);
		}

		// Schedule progress reset if needed.
		if (wasSeeking && !Seeking)
			Invoke("ResetProgress", Random.Range(ProgressResetMinTime, ProgressResetMaxTime));
		
		// Update the checkpoint.
		if (Seeking)
			ProgressCheckpoint = d;
	}
	
	/** Reset progress and try seeking again. */
	private void ResetProgress() {
		Seeking = true;
		ProgressCheckpoint = float.MaxValue;
	}
	
	/** Returns distance to target. */
	public float TargetDistance() {
		if (!Target)
			return float.MaxValue;
		else
			return Vector3.Distance(Target.transform.position, t.position);
	}
	
}
